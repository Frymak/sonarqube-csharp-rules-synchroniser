/* eslint-disable @typescript-eslint/naming-convention */
let TYPES = {
    IFileSystem: Symbol("IFileSystem"),
    IProjectUpdater: Symbol("IProjectUpdater"),
    IVsCodeConfiguration: Symbol("IVsCodeConfiguration"),
    IUIInteractions: Symbol("IUIInteractions"),
    ILogger: Symbol("ILogger"),
    ISonarQubeClient: Symbol("ISonarQubeClient"),
    IOmniSharpUpdater: Symbol("IOmniSharpUpdater"),
    IAnalyzersInstaller: Symbol("IAnalyzersInstaller"),
    IRulesetFileIO: Symbol("IRulesetFileIO"),
    IUnzipper: Symbol("IUnzipper"),
    IExtensionEntryPoint: Symbol("IExtensionEntryPoint") // composition root
};

export default TYPES;