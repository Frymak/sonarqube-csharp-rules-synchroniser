export interface IOmniSharpUpdater{
    update(analyzersPath:string, isUpdating:boolean):void;
}