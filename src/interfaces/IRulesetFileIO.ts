export interface IRulesetFileIO{
    getConfiguredPath(projFile:string):string;
    getFullPath(qualityProfileName:string):string;
    write(qualityProfileName:string, content:string):void;
    getDefaultFileName():string;
}