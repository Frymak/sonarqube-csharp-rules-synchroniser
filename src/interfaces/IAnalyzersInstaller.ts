export interface IAnalyzersInstaller{
    install(callback:(analyserInstalled:boolean|undefined, analyzersPath:string, isUpdating:boolean)=>void, updateIfAlreadyInstalled:boolean):void;
    cleanAtStart():void;
}