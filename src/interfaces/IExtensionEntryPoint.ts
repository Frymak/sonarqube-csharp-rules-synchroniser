export interface IExtensionEntryPoint { 
    synchronizeCommand():void;
    installOrUpdateAnalyzersCommand():void;
}