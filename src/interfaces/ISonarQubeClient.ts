export interface ISonarQubeClient{
    synchronizeQualityRules(sonarQubeProjectkey:string, callback:()=>void):void;
}