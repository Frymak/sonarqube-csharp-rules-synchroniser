import { ILogger } from './utils/ILogger';
import TYPES from './Types';
import { inject, injectable } from 'inversify';
import { IVsCodeConfiguration } from './utils/IVsCodeConfiguration';
import { IOmniSharpUpdater } from './interfaces/IOmniSharpUpdater';

const homedir = require('os').homedir();

@injectable()
export class OmniSharpUpdater implements IOmniSharpUpdater{
	private _configuration: IVsCodeConfiguration;
	private _logger:ILogger;

	constructor(@inject(TYPES.IVsCodeConfiguration)configuration: IVsCodeConfiguration,
				@inject(TYPES.ILogger) logger:ILogger){
		this._logger = logger;
		this._configuration = configuration;
	}

    update(analyzersPath:string, isUpdating:boolean){
        if(!this._configuration.getEnableRoslynAnalyzersOmniSharpConfiguration()){
			this._configuration.updateEnableRoslynAnalyzersOmniSharpConfiguration(true, true);
		}
		
		const omnisharpJson = this._configuration.getOrCreateOmniSharpJsonConfigurationContent();
		
		if(!omnisharpJson.RoslynExtensionsOptions.EnableAnalyzersSupport){
			omnisharpJson.RoslynExtensionsOptions.EnableAnalyzersSupport = true;
		}

		const locations:string[] = omnisharpJson.RoslynExtensionsOptions.LocationPaths;
		if(isUpdating){
			const previousPath = locations.find(v => v.toLowerCase().includes('sonaranalyzer.csharp'));
			if(previousPath !== undefined){
				// remove previous configuration for sonarnalyzer.csharp
				locations.splice(locations.indexOf(previousPath));
			}
		}
		
		if(!locations.some(v => v.toLowerCase().includes('sonaranalyzer.csharp'))){
			locations.push(analyzersPath);
			this._configuration.updateOmniSharpJsonConfigurationContent(omnisharpJson, (omnisharpJsonFilepath:string)=>{
			    this._logger.appendLine('Omnisharp analyzers list updated in file: ' + omnisharpJsonFilepath);
			});			
		}
    }
}