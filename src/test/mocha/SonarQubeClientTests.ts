import * as chai from 'chai';
import { Container } from "inversify";
import * as jsonServer from 'json-server';
import { IRulesetFileIO } from '../../interfaces/IRulesetFileIO';
import { ISonarQubeClient } from '../../interfaces/ISonarQubeClient';
import { SonarQubeClient } from '../../SonarQubeClient';
import TYPES from "../../Types";
import { IFileSystem } from "../../utils/IFileSystem";
import { ILogger } from "../../utils/ILogger";
import { IUIInteractions } from '../../utils/IUIInteractions';
import { IVsCodeConfiguration } from "../../utils/IVsCodeConfiguration";
import { RulesetFileIO } from '../../utils/RulesetFileIO';
import { NullLogger } from "./mock/NullLogger";
import { MockFileSystem } from './mock/MockFileSystem';
import { TestsConfiguration } from "./mock/TestConfiguration";
import { TestUIInteractions } from './mock/TestUIInteractions';

describe('SonarQubeClient tests', function() {
    describe('synchronizeQualityRules()', function() {
        it('should write locally quality rules file', function(done) {
            this.timeout(150); // double the default value (75ms) because locally this test already reaches 50ms.

            // ARRANGE
            const container = initializeGenericTestsIoC();
            const projectKey = 'myProjectKey';
            const qualityProfileName = 'MyQualityProfileName';
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            const fileSystem = container.get<IFileSystem>(TYPES.IFileSystem) as MockFileSystem;
            configuration.sonarQubeProjectkey = projectKey;
            configuration.sonarQubeRootUrl = 'http://localhost:63011'; // todo (3000 previously)
            configuration.workspaceRootFolderUri = './';

            const server = jsonServer.create();
            let routesDB = new Object();
            const router = jsonServer.router(routesDB);

            server.get("/api/qualityprofiles/search" , (req,res) =>  // ?format=json&language=cs&project=" + projectKey, (req,res) => 
                res.status(200).header("content-type", "application/json").json({ "profiles":[{
                        "key":"myKey","name":qualityProfileName, "language":"cs","languageName":"C#","isInherited":false,"isDefault":true,
                        "activeRuleCount":119,"activeDeprecatedRuleCount":14,"rulesUpdatedAt":"2021-01-13T15:54:59+0000",
                        "lastUsed":"2021-02-22T18:00:46+0000","userUpdatedAt":"2021-01-13T15:54:59+0000","isBuiltIn":false,
                        "actions":{"edit":true,"setAsDefault":false,"copy":true,"associateProjects":false,"delete":false}
                    }],
                    "actions":{"create":true}}
                ));

            const xml:string = '<?xml version="1.0" encoding="utf-8"?><RuleSet Name="Rules for SonarLint" Description="This rule set was automatically generated from SonarQube." ToolsVersion="14.0"><Rules AnalyzerId="SonarAnalyzer.CSharp" RuleNamespace="SonarAnalyzer.CSharp"><Rule Id="S3869" Action="Warning" /><Rule Id="S2092" Action="Warning" /><Rule Id="S3060" Action="Warning" /><Rule Id="S4275" Action="Warning" /><Rule Id="S4834" Action="Warning" /><Rule Id="S3877" Action="Warning" />';

            server.get("/api/qualityprofiles/export" , (req,res) =>  // ?language=cs&exporterKey=sonarlint-vs-cs&qualityProfile=" + qualityProfileName, (req,res) => 
                res.status(200).header("content-type", "text/plain")
                .send(xml)
            );

            server.use(router);
            const sonarClient:ISonarQubeClient = container.get<ISonarQubeClient>(TYPES.ISonarQubeClient);

            const listener = server.listen(63011, () => {   // 3000 previously
                console.log('JSON Server is running');              
                let callbackCalled:boolean = false;

                // ACT
                sonarClient.synchronizeQualityRules(projectKey, () => 
                {
                    chai.assert.isNotNull(fileSystem.writtenFiles);
                    chai.assert.isNotNull(fileSystem.writtenFiles['.//.sonarlint/myprojectkeycsharp.ruleset']);
                    chai.assert.strictEqual(fileSystem.writtenFiles['.//.sonarlint/myprojectkeycsharp.ruleset'], xml);
                    listener.close();
                    done();
                });
            });
        });
    });
});

function initializeGenericTestsIoC():Container {
    const result = new Container();
    result.bind<ILogger>(TYPES.ILogger).to(NullLogger);
    result.bind<IFileSystem>(TYPES.IFileSystem).to(MockFileSystem).inSingletonScope();
    result.bind<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration).to(TestsConfiguration).inSingletonScope();
    result.bind<IUIInteractions>(TYPES.IUIInteractions).to(TestUIInteractions).inSingletonScope();
    result.bind<IRulesetFileIO>(TYPES.IRulesetFileIO).to(RulesetFileIO).inSingletonScope();
    result.bind<ISonarQubeClient>(TYPES.ISonarQubeClient).to(SonarQubeClient).inSingletonScope();
    return result;
  }