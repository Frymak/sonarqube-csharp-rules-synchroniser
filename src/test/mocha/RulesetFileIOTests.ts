import * as chai from 'chai';
import { Container } from "inversify";
import { IRulesetFileIO } from "../../interfaces/IRulesetFileIO";
import TYPES from "../../Types";
import { IFileSystem } from "../../utils/IFileSystem";
import { ILogger } from "../../utils/ILogger";
import { IVsCodeConfiguration } from "../../utils/IVsCodeConfiguration";
import { RulesetFileIO } from "../../utils/RulesetFileIO";
import { NullLogger } from "./mock/NullLogger";
import { MockFileSystem } from './mock/MockFileSystem';
import { TestsConfiguration } from "./mock/TestConfiguration";

describe('RulesetFileIO tests', function() {
    describe('getDefaultFileName()', function() {
      it('should add correct suffix one default filename', function() {
        // ARRANGE
        const container = initializeGenericTestsIoC();
        let rulesetFileIO:IRulesetFileIO = container.get<IRulesetFileIO>(TYPES.IRulesetFileIO);
        const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
        // please let this name in lowercase to avoid misbehavior (we don't want to test case changes on this unit test)
        configuration.sonarQubeProjectkey = 'myprojectkey';
        configuration.workspaceRootFolderUri = './';

        const expectedFilename = configuration.sonarQubeProjectkey + 'csharp.ruleset';

        // ACT
        const filename = rulesetFileIO.getDefaultFileName();

        // ASSERT
        chai.assert.strictEqual(filename, expectedFilename);
      });

      it('should set project name in lowercase and replace special chars ', function() {
        // ARRANGE
        const container = initializeGenericTestsIoC();
        let rulesetFileIO:IRulesetFileIO = container.get<IRulesetFileIO>(TYPES.IRulesetFileIO);
        const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
        // please let this name in lowercase to avoid misbehavior (we don't want to test case changes on this unit test)
        configuration.sonarQubeProjectkey = 'm' + "\\" + 'yP<>ro|je:*?"c/tKey';
        configuration.workspaceRootFolderUri = './';

        const expectedFilename = 'm_yp__ro_je____c_tkeycsharp.ruleset';

        // ACT
        const filename = rulesetFileIO.getDefaultFileName();

        // ASSERT
        chai.assert.strictEqual(filename, expectedFilename);
      });
    });
    describe('getFullPath()', function() {
        it('should return default sonarlint relative path when sonarlint mode is enabled', function() {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            let rulesetFileIO:IRulesetFileIO = container.get<IRulesetFileIO>(TYPES.IRulesetFileIO);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            // please let this name in lowercase to avoid misbehavior (we don't want to test case changes on this unit test)
            configuration.sonarQubeProjectkey = 'myprojectkey';
            configuration.sonarlintCompatibilityMode = true;
            configuration.workspaceRootFolderUri = './myWorkspaceFolder';

            const expectedFullPath = configuration.workspaceRootFolderUri + '/.sonarlint/' + configuration.sonarQubeProjectkey + 'csharp.ruleset';
            const qualityProfileName = 'dummyQualityProfileName';

            // ACT
            const fullPath = rulesetFileIO.getFullPath(qualityProfileName);

            // ASSERT
            chai.assert.strictEqual(fullPath, expectedFullPath);
        });

        it('should return configured relative path when sonarlint mode is disabled', function() {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            let rulesetFileIO:IRulesetFileIO = container.get<IRulesetFileIO>(TYPES.IRulesetFileIO);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            // please let this name in lowercase to avoid misbehavior (we don't want to test case changes on this unit test)
            configuration.sonarQubeProjectkey = 'myprojectkey';
            configuration.sonarlintCompatibilityMode = false;
            configuration.rulesetRelativepath = 'myrelativepath/path.ruleset';
            configuration.workspaceRootFolderUri = './myWorkspaceFolder';

            const expectedFullPath = configuration.workspaceRootFolderUri + '/' + configuration.rulesetRelativepath;
            const qualityProfileName = 'dummyQualityProfileName';

            // ACT
            const fullPath = rulesetFileIO.getFullPath(qualityProfileName);

            // ASSERT
            chai.assert.strictEqual(fullPath, expectedFullPath);
        });

        it('should return default relative path with QualityProfileName when sonarlint mode is disabled and no rulesetRelative path configured', function() {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            let rulesetFileIO:IRulesetFileIO = container.get<IRulesetFileIO>(TYPES.IRulesetFileIO);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            // please let this name in lowercase to avoid misbehavior (we don't want to test case changes on this unit test)
            configuration.sonarQubeProjectkey = 'myprojectkey';
            configuration.sonarlintCompatibilityMode = false;
            configuration.rulesetRelativepath = undefined;
            configuration.workspaceRootFolderUri = './myWorkspaceFolder';

            const qualityProfileName = 'dummyQualityProfileName';
            const expectedFullPath = configuration.workspaceRootFolderUri + '/' + qualityProfileName + '.ruleset';

            // ACT
            const fullPath = rulesetFileIO.getFullPath(qualityProfileName);

            // ASSERT
            chai.assert.strictEqual(fullPath, expectedFullPath);
        });
    });

    describe('getConfiguredPath()', function() {
        it('should use MSBuildThisFileDirectory variable in the value when Sonarlint compatibility mode disabled', function() {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            let rulesetFileIO:IRulesetFileIO = container.get<IRulesetFileIO>(TYPES.IRulesetFileIO);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            // please let this name in lowercase to avoid misbehavior (we don't want to test case changes on this unit test)
            configuration.sonarQubeProjectkey = 'myprojectkey';
            configuration.sonarlintCompatibilityMode = false;
            configuration.rulesetRelativepath = 'myrelativepath/path.ruleset';
            configuration.workspaceRootFolderUri = './myWorkspaceFolder';

            const projectFile = 'c:\test\myprojectFilePath.csproj';
            const expectedConfiguredPath = '$(MSBuildThisFileDirectory)\\' + configuration.rulesetRelativepath;

            // ACT
            const path = rulesetFileIO.getConfiguredPath(projectFile);

            // ASSERT
            chai.assert.strictEqual(path, expectedConfiguredPath);
        });

        it('should use relative path notation in the value when Sonarlint compatibility mode enabled', function() {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            let rulesetFileIO:IRulesetFileIO = container.get<IRulesetFileIO>(TYPES.IRulesetFileIO);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            // please let this name in lowercase to avoid misbehavior (we don't want to test case changes on this unit test)
            configuration.sonarQubeProjectkey = 'myprojectkey';
            configuration.sonarlintCompatibilityMode = true;
            configuration.rulesetRelativepath = 'myrelativepath/path.ruleset';
            configuration.workspaceRootFolderUri = 'c:\\test\\';

            const projectFile = 'c:\\test\\folderRelative1\\folderRelative2/folderRelative3\\myprojectFilePath.csproj';
            const expectedConfiguredPath = '../../.sonarlint/' + configuration.sonarQubeProjectkey + 'csharp.ruleset';

            // ACT
            const path = rulesetFileIO.getConfiguredPath(projectFile);

            // ASSERT
            chai.assert.strictEqual(path, expectedConfiguredPath);
            /*let tmpdir = require('os').tmpdir();
            console.warn(tmpdir);*/
        });
    });
});

function initializeGenericTestsIoC():Container {
    const result = new Container();
    result.bind<ILogger>(TYPES.ILogger).to(NullLogger);
    result.bind<IFileSystem>(TYPES.IFileSystem).to(MockFileSystem).inSingletonScope();
    result.bind<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration).to(TestsConfiguration).inSingletonScope();
    result.bind<IRulesetFileIO>(TYPES.IRulesetFileIO).to(RulesetFileIO).inSingletonScope();
    return result;
  }