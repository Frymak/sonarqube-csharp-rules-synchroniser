import * as chai from 'chai';
import * as jsonServer from 'json-server';
import { Container, injectable } from "inversify";
import { IAnalyzersInstaller } from '../../interfaces/IAnalyzersInstaller';
import { SonarRoslynAnalyzersInstaller } from '../../SonarRoslynAnalyzersInstaller';
import TYPES from "../../Types";
import { IFileSystem } from "../../utils/IFileSystem";
import { ILogger } from "../../utils/ILogger";
import { IUIInteractions } from '../../utils/IUIInteractions';
import { IVsCodeConfiguration } from "../../utils/IVsCodeConfiguration";
import { NullLogger } from "./mock/NullLogger";
import { MockFileSystem } from './mock/MockFileSystem';
import { TestsConfiguration } from "./mock/TestConfiguration";
import { TestUIInteractions } from './mock/TestUIInteractions';
import { Constants } from '../../Constants';
import { IUnzipper } from '../../utils/IUnzipper';
import { ParseOptions } from 'unzipper';
import { Unzipper } from '../../utils/Unzipper';
import { Duplex } from 'stream';
const homedir = require('os').homedir();

describe('SonarRoslynAnalyzersInstaller tests', function() {
    describe('install()', function() {
        it('should install sonar roslyn analyzers and be able to retrieve real nugets #integration-tests', function(done) {
            this.timeout(2000); // as we are doing real http call to nuget org during this test, we need more time...
            
            // ARRANGE
            const container = initializeGenericTestsIoC();
            const fileSystem = container.get<IFileSystem>(TYPES.IFileSystem) as MockFileSystem;
            fileSystem.mockedDirectories[homedir + Constants.pluginPath] = ['']; // simulate no analyzers installed
            fileSystem.existsFunc = () => false;

            const unzipper = container.get<IUnzipper>(TYPES.IUnzipper) as MockUnzipper;
            
            // ACT
            const analyzersInstaller:IAnalyzersInstaller = container.get<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller);
            analyzersInstaller.install((analysersInstalled, analyzersPath, isUpdating) => {
                
                // ASSERT
                chai.assert.isTrue(analysersInstalled);
                chai.assert.isFalse(isUpdating);
                chai.assert.isAtLeast(unzipper.extractedTargetPath?.length ?? 0, 1);
                chai.assert.isTrue(unzipper.extractedTargetPath?.startsWith(homedir + Constants.pluginPath + 'sonaranalyzer.csharp.'));
                chai.assert.strictEqual(analyzersPath.replace(/\\/g, '/'), unzipper.extractedTargetPath?.replace(/\\/g, '/') + '/analyzers');
                done();
            }, false);
        });

        it('should returns correct analyzers path if sonar roslyn analyzers already installed and update parameter set to false', function(done) {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            const fileSystem = container.get<IFileSystem>(TYPES.IFileSystem) as MockFileSystem;
            const relativeAnalyzersPath = 'sonaranalyzer.csharp.8.18.0.27296';
            fileSystem.mockedDirectories[homedir + Constants.pluginPath] = [relativeAnalyzersPath];
            fileSystem.existsFunc = () => true;

            // ACT
            const analyzersInstaller:IAnalyzersInstaller = container.get<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller);
            analyzersInstaller.install((analysersInstalled, analyzersPath, isUpdating) => {

                // ASSERT
                chai.assert.strictEqual(analyzersPath, homedir + Constants.pluginPath + relativeAnalyzersPath + '/analyzers');
                done();
            }, false);
        });

        it('should do nothing if sonar roslyn analyzers already installed and update parameter set to false', function(done) {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            const fileSystem = container.get<IFileSystem>(TYPES.IFileSystem) as MockFileSystem;
            const relativeAnalyzersPath = 'sonaranalyzer.csharp.8.18.0.27296';
            fileSystem.mockedDirectories[homedir + Constants.pluginPath] = [relativeAnalyzersPath];
            fileSystem.existsFunc = () => true;

            // ACT
            const analyzersInstaller:IAnalyzersInstaller = container.get<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller);
            analyzersInstaller.install((analysersInstalled, analyzersPath, isUpdating) => {
                // ASSERT
                chai.assert.isFalse(analysersInstalled);
                chai.assert.isAtMost(fileSystem.createdDirectories.length, 0);
                done();
            }, false);
        });

        it('should do nothing if latest sonar roslyn analyzers already installed and update parameter set to true', function(done) {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            const fileSystem = container.get<IFileSystem>(TYPES.IFileSystem) as MockFileSystem;
            const relativeAnalyzersPath = 'sonaranalyzer.csharp.8.19.0.28253.nupkg'; // for tests on previous version, we can use 'sonaranalyzer.csharp.8.18.0.27296'
            fileSystem.mockedDirectories[homedir + Constants.pluginPath] = [relativeAnalyzersPath];
            fileSystem.existsFunc = () => true;

            const server = jsonServer.create();
            let routesDB = new Object();
            const router = jsonServer.router(routesDB);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            configuration.nugetOrgRootUrl = "http://localhost:63011";

            // when calling this uri, it returns a redirect response and it's from this Uri we have the version (from file name) 
            // so we simulate this redirect response behavior
            server.get("/api/v2/package/SonarAnalyzer.CSharp/" , (req,res) =>  
                res.status(302).header("content-type", "text/html").location("http://localhost:63011/packages/sonaranalyzer.csharp.8.19.0.28253.nupkg").send());

            server.use(router);
            const listener = server.listen(63011, () => {   // 3000 previously
                console.log('JSON Server is running');              
                let callbackCalled:boolean = false;

                try{
                // ACT
                 // ACT
                const analyzersInstaller:IAnalyzersInstaller = container.get<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller);
                analyzersInstaller.install((analysersInstalled, analyzersPath, isUpdating) => {
                    // ASSERT
                    chai.assert.isFalse(isUpdating);
                    chai.assert.isFalse(analysersInstalled);
                    chai.assert.isAtMost(fileSystem.createdDirectories.length, 0);
                    listener.close();
                    done();
                }, true);
                }
                catch(ex){
                    listener.close();
                    chai.assert.fail(ex);
                    done();
                }
            });           
        });

        it('should update if latest sonar roslyn analyzers not installed and update parameter set to true', function(done) {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            const fileSystem = container.get<IFileSystem>(TYPES.IFileSystem) as MockFileSystem;
            const relativeAnalyzersPath = 'sonaranalyzer.csharp.8.18.0.27296'; // for tests on previous version, we can use 'sonaranalyzer.csharp.8.18.0.27296'
            const relativeNewPackage = 'sonaranalyzer.csharp.8.19.0.28253.nupkg';
            const fullPluginPath = homedir + Constants.pluginPath;
            fileSystem.mockedDirectories[fullPluginPath] = [relativeAnalyzersPath];
            fileSystem.existsFunc = (path:string | void) => {
                if(path !== undefined) {
                    if(path.includes('sonaranalyzer.csharp.8.18.0.27296') || path === fullPluginPath){
                        return true;
                    }
                } 
                return false;
            };
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            configuration.nugetOrgRootUrl = "http://localhost:63011";

            const server = jsonServer.create();
            let routesDB = new Object();
            const router = jsonServer.router(routesDB);

            // when calling this uri, it returns a redirect response and it's from this Uri we have the version (from file name) 
            // so we simulate this redirect response behavior
            server.get("/api/v2/package/SonarAnalyzer.CSharp/" , (req,res) =>  
                res.status(302).header("content-type", "text/html").location("http://localhost:63011/packages/sonaranalyzer.csharp.8.19.0.28253.nupkg").send());
            
            server.get("/packages/" + relativeNewPackage, (req,res) =>  
                res.status(200).header("content-type", "text/html").send());

            server.use(router);
            const listener = server.listen(63011, () => {   // 3000 previously
                console.log('JSON Server is running');              
                let callbackCalled:boolean = false;
                // ACT
                const analyzersInstaller:IAnalyzersInstaller = container.get<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller);
                try{
                    analyzersInstaller.install((analysersInstalled, analyzersPath, isUpdating) => {
                        // ASSERT
                        chai.assert.isTrue(isUpdating);
                        chai.assert.isTrue(analysersInstalled);
                        chai.assert.isAtMost(fileSystem.createdDirectories.length, 0);
                        chai.assert.strictEqual(fileSystem.writtenFiles[fullPluginPath + '/.todelete'], fullPluginPath + '/' + relativeAnalyzersPath);
                        chai.assert.isNotNull(fileSystem.writtenFiles[fullPluginPath + relativeNewPackage]);
                        listener.close();
                        done();
                    }, true);
                }
                catch(ex){
                    listener.close();
                    chai.assert.fail(ex);
                    done();
                }
            });
        });
    });
    
    describe('cleanAtStart()', function() {
        it('should clean file marked as to be deleted at start', function() {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            configuration.omniSharpJsonConfigurationContent = 
                // eslint-disable-next-line @typescript-eslint/naming-convention
                {"RoslynExtensionsOptions": {
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    "EnableAnalyzersSupport": true,
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        "LocationPaths": [
                            homedir + Constants.pluginPath + "sonaranalyzer.csharp.8.19.0.28253/analyzers"
                        ]}
                };
            const fileSystem = container.get<IFileSystem>(TYPES.IFileSystem) as MockFileSystem;
            const relativeAnalyzersPath = 'sonaranalyzer.csharp.8.19.0.28253';
            fileSystem.mockedDirectories[homedir + Constants.pluginPath] = [relativeAnalyzersPath];
            fileSystem.existsFunc = () => true;
            const toDeleteRelativePath = '/.todelete';
            const toDeleteContent = 'myTestPath/to/delete';
            fileSystem.readFileFunc = (path:string) => { 
                if(path.endsWith(toDeleteRelativePath)){
                    return Buffer.from(toDeleteContent);
                }
                return Buffer.from('');
            };

            // ACT
            const analyzersInstaller:IAnalyzersInstaller = container.get<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller);
            analyzersInstaller.cleanAtStart();
            
            // ASSERT
            chai.assert.strictEqual(fileSystem.deletedItems.length,2);
            chai.assert.strictEqual(fileSystem.deletedItems[0],toDeleteContent);
            chai.assert.strictEqual(fileSystem.deletedItems[1], homedir + Constants.pluginPath + toDeleteRelativePath);
        });

        it('should clean unused sonar analysers folders', function() {
            // ARRANGE
            const container = initializeGenericTestsIoC();
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            configuration.omniSharpJsonConfigurationContent = 
                // eslint-disable-next-line @typescript-eslint/naming-convention
                {"RoslynExtensionsOptions": {
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    "EnableAnalyzersSupport": true,
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        "LocationPaths": [
                        ]}
                };
            const fileSystem = container.get<IFileSystem>(TYPES.IFileSystem) as MockFileSystem;
            const relativeAnalyzersPath = 'sonaranalyzer.csharp.8.18.0.27296';
            fileSystem.mockedDirectories[homedir + Constants.pluginPath] = [relativeAnalyzersPath];
            fileSystem.existsFunc = (path) => path !== undefined && path.includes(relativeAnalyzersPath);

            // ACT
            const analyzersInstaller:IAnalyzersInstaller = container.get<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller);
            analyzersInstaller.cleanAtStart();
            
            // ASSERT
            chai.assert.strictEqual(fileSystem.deletedItems.length,1);
            chai.assert.strictEqual(fileSystem.deletedItems[0],homedir + Constants.pluginPath + relativeAnalyzersPath);
        });
    });
});

function initializeGenericTestsIoC():Container {
    const result = new Container();
    result.bind<ILogger>(TYPES.ILogger).to(NullLogger);
    result.bind<IFileSystem>(TYPES.IFileSystem).to(MockFileSystem).inSingletonScope();
    result.bind<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration).to(TestsConfiguration).inSingletonScope();
    result.bind<IUIInteractions>(TYPES.IUIInteractions).to(TestUIInteractions).inSingletonScope();
    result.bind<IUnzipper>(TYPES.IUnzipper).to(MockUnzipper).inSingletonScope();
    result.bind<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller).to(SonarRoslynAnalyzersInstaller).inSingletonScope();
    return result;
  }

@injectable()
class MockUnzipper extends Unzipper{
    public extractedTargetPath:string|undefined = undefined;
    extract(opts?: ParseOptions): Duplex {
        this.extractedTargetPath = opts?.path;
        return new Duplex({
            write(){},
            read(){}
        });
    }
}