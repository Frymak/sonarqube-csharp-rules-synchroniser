import * as chai from 'chai';
import { Container, injectable } from 'inversify';
import { ExtensionEntryPoint } from '../../ExtensionEntryPoint';
import { IAnalyzersInstaller } from '../../interfaces/IAnalyzersInstaller';
import { IExtensionEntryPoint } from '../../interfaces/IExtensionEntryPoint';
import { IOmniSharpUpdater } from '../../interfaces/IOmniSharpUpdater';
import { IRulesetFileIO } from '../../interfaces/IRulesetFileIO';
import { ISonarQubeClient } from '../../interfaces/ISonarQubeClient';
import { SonarQubeClient } from '../../SonarQubeClient';
import TYPES from '../../Types';
import { FileSystem } from '../../utils/FileSystem';
import { IFileSystem } from '../../utils/IFileSystem';
import { ILogger } from '../../utils/ILogger';
import { IUIInteractions } from '../../utils/IUIInteractions';
import { IUnzipper } from '../../utils/IUnzipper';
import { IVsCodeConfiguration } from '../../utils/IVsCodeConfiguration';
import { RulesetFileIO } from '../../utils/RulesetFileIO';
import { Unzipper } from '../../utils/Unzipper';
import { NullLogger } from './mock/NullLogger';
import { TestsConfiguration } from './mock/TestConfiguration';
import { TestUIInteractions } from './mock/TestUIInteractions';


describe('ExtensionEntryPoint tests', function() {
  describe('synchronizeCommand()', function() {
    it('should call nothing and stop on bad configuration', function() {
      
      const container = initializeGenericTestsIoC();
      let sonarApiWasCalled:boolean = false;
      const client:ISonarQubeClient = new TestSonarQubeClient((a)=>{sonarApiWasCalled = true;});  
      container.bind<ISonarQubeClient>(TYPES.ISonarQubeClient).toConstantValue(client);

      let actualRelatedPath: string|undefined;
      let updaterWasCalled:boolean = false;
      const updater = new TestProjectUpdater(() => {updaterWasCalled = true;});
      container.bind<IProjectUpdater>(TYPES.IProjectUpdater).toConstantValue(updater);
      const ep:IExtensionEntryPoint = container.get<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint);

      const expectedRelativePath = "testPath";
      const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
      configuration.rulesetRelativepath = expectedRelativePath;

      let currentErrorMessage = '';
      const interactions = container.get<IUIInteractions>(TYPES.IUIInteractions) as TestUIInteractions;
      interactions.errorCallBack = message => currentErrorMessage = message;

      ep.synchronizeCommand();
      chai.assert(!updaterWasCalled, "Project Updater code was called. When configuration is not ok, nothing should be called.");
      chai.assert(!sonarApiWasCalled, "Sonar Client Api code was called. When configuration is not ok, nothing should be called.");
      chai.assert(currentErrorMessage.length > 0, "When no configuration provided, an UI error message should be displayed.");
    });
    it('should call SonarQubeClient and projectUpdater', function() {
      
      const container = initializeGenericTestsIoC();
      let actualProjectKey: string|undefined;
      const client:ISonarQubeClient = new TestSonarQubeClient((projectKey)=>{ actualProjectKey = projectKey; });  
      container.bind<ISonarQubeClient>(TYPES.ISonarQubeClient).toConstantValue(client);

      let updaterWasCalled:boolean = false;
      const updater = new TestProjectUpdater(() => {updaterWasCalled = true;});
      container.bind<IProjectUpdater>(TYPES.IProjectUpdater).toConstantValue(updater);
      const ep:IExtensionEntryPoint = container.get<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint);

      const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
      configuration.sonarQubeProjectkey = 'myprojectkey';
      configuration.sonarQubeRootUrl = 'http://localhost:9000/';
      configuration.sonarQubeToken = 'dummyToken';
      configuration.workspaceRootFolderUri = './';

      ep.synchronizeCommand();
      chai.assert.strictEqual(actualProjectKey, configuration.sonarQubeProjectkey);
      chai.assert.isTrue(updaterWasCalled);
    });
    it('should manage unattended exceptions', function(){
      // ARRANGE
      const container = initializeGenericTestsIoC();
      container.bind<ISonarQubeClient>(TYPES.ISonarQubeClient).to(ExceptionSonarQubeClient).inSingletonScope();
      container.bind<IProjectUpdater>(TYPES.IProjectUpdater).to(TestProjectUpdater);
      const ep:IExtensionEntryPoint = container.get<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint);

      const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
      configuration.sonarQubeProjectkey = 'myprojectkey';
      configuration.sonarQubeRootUrl = 'http://localhost:9000/';
      configuration.sonarQubeToken = 'dummyToken';
      configuration.workspaceRootFolderUri = './';


      // ACT & ASSERT (we expect no exception even if no instances injected in IoC)
      chai.assert.doesNotThrow(() => 
      {
        ep.synchronizeCommand();});
    });
  });
  describe('installOrUpdateAnalyzersCommand()', function() {
    it('should clean at start, install analyzers and update omnisharp configuration', function() {
      // ARRANGE
      const container = initializeGenericTestsIoC();
      container.bind<IProjectUpdater>(TYPES.IProjectUpdater).to(TestProjectUpdater);
      container.bind<ISonarQubeClient>(TYPES.ISonarQubeClient).to(SonarQubeClient);
      const ep:IExtensionEntryPoint = container.get<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint);
      const analyzersInstaller:MockAnalyzersInstaller = container.get<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller) as MockAnalyzersInstaller;
      const omnisharpUpdater:MockOmnisharpUpdater = container.get<IOmniSharpUpdater>(TYPES.IOmniSharpUpdater) as MockOmnisharpUpdater;

      // ACT
      ep.installOrUpdateAnalyzersCommand();

      // ASSERT
      chai.assert.isTrue(analyzersInstaller.cleanAtStartWasCalled);
      chai.assert.isTrue(analyzersInstaller.installWasCalled);
      chai.assert.isTrue(omnisharpUpdater.omnisharpUpdaterWasCalled);
    });
    it('should manage unattended exceptions', function() {
      // ARRANGE
      const container = initializeGenericTestsIoC();
      container.bind<IProjectUpdater>(TYPES.IProjectUpdater).to(TestProjectUpdater);
      container.bind<ISonarQubeClient>(TYPES.ISonarQubeClient).to(SonarQubeClient);
      const ep:IExtensionEntryPoint = container.get<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint);
      container.unbind(TYPES.IAnalyzersInstaller);
      container.bind<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller).to(ExceptionAnalyzersInstaller);
      

      // ACT & ASSERT
      chai.assert.doesNotThrow(() => ep.installOrUpdateAnalyzersCommand());      
    });
  });
});

function initializeGenericTestsIoC():Container {
  const result = new Container();
  result.bind<IUIInteractions>(TYPES.IUIInteractions).to(TestUIInteractions).inSingletonScope();
	result.bind<ILogger>(TYPES.ILogger).to(NullLogger);
  result.bind<IFileSystem>(TYPES.IFileSystem).to(FileSystem).inSingletonScope();
  result.bind<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint).to(ExtensionEntryPoint);
  // do not bind it here cause we bind it during tests: result.bind<ISonarQubeClient>(TYPES.ISonarQubeClient).to(TestSonarQubeClient).inSingletonScope();
	result.bind<IOmniSharpUpdater>(TYPES.IOmniSharpUpdater).to(MockOmnisharpUpdater).inSingletonScope();
	result.bind<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller).to(MockAnalyzersInstaller).inSingletonScope();
  result.bind<IRulesetFileIO>(TYPES.IRulesetFileIO).to(RulesetFileIO).inSingletonScope();
  result.bind<IUnzipper>(TYPES.IUnzipper).to(Unzipper).inSingletonScope();
  result.bind<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration).to(TestsConfiguration).inSingletonScope();
    
	return result;
}

@injectable()
  class TestSonarQubeClient implements ISonarQubeClient{
  private synchronizeQualityRulesCallback:(sonarQubeProjectkey: string) => void;
  
  constructor(synchronizeQualityRulesCallback:(sonarQubeProjectkey: string) => void){
    this.synchronizeQualityRulesCallback = synchronizeQualityRulesCallback;
  }

  synchronizeQualityRules(sonarQubeProjectkey: string, callback: () => void): void {
    this.synchronizeQualityRulesCallback(sonarQubeProjectkey);
    callback();
  }
}

@injectable()
class MockAnalyzersInstaller implements IAnalyzersInstaller{
  public installWasCalled = false;
  public cleanAtStartWasCalled = false;
  install(callback: (analyserInstalled: boolean | undefined, analyzersPath: string, isUpdating: boolean) => void, updateIfAlreadyInstalled: boolean): void {
    callback(false, 'testPath', false);
    this.installWasCalled = true;
  }
  cleanAtStart(): void {
    this.cleanAtStartWasCalled = true;
  }
}
@injectable()
class MockOmnisharpUpdater implements IOmniSharpUpdater{
  public omnisharpUpdaterWasCalled = false;
  update(analyzersPath: string, isUpdating: boolean): void {
    this.omnisharpUpdaterWasCalled = true;
  }
}
@injectable()
class TestProjectUpdater implements IProjectUpdater{
  private updateCallback:() => void;
  constructor(updateCallback:() => void = () => {}){
    this.updateCallback = updateCallback;
  }

  update(): void {
    this.updateCallback();
  }

  setSonarLintCompatibilityMode(enabled: boolean, projectkey: string | undefined): void {
  }  
}

@injectable()
class ExceptionSonarQubeClient implements ISonarQubeClient{
  synchronizeQualityRules(sonarQubeProjectkey: string, callback: () => void): void {
    throw new Error('Method not implemented.');
  }  
}
@injectable()
class ExceptionAnalyzersInstaller implements IAnalyzersInstaller{
  install(callback: (analyserInstalled: boolean | undefined, analyzersPath: string, isUpdating: boolean) => void, updateIfAlreadyInstalled: boolean): void {
    throw new Error('Method not implemented.');
  }
  cleanAtStart(): void {
    throw new Error('Method not implemented.');
  }  
}
