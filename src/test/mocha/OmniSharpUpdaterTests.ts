import * as chai from 'chai';
import { Container } from "inversify";
import { IOmniSharpUpdater } from '../../interfaces/IOmniSharpUpdater';
import { OmniSharpUpdater } from '../../OmniSharpUpdater';
import TYPES from "../../Types";
import { ILogger } from "../../utils/ILogger";
import { IVsCodeConfiguration } from "../../utils/IVsCodeConfiguration";
import { NullLogger } from "./mock/NullLogger";
import { TestsConfiguration } from "./mock/TestConfiguration";

describe('OmniSharpUpdater tests', function() {
    describe('update()', function() {
        it('should add location when no one is set', function() {
            // ARANGE
            initializeGenericTestsIoC();
            const container = initializeGenericTestsIoC();
            let omniSharpUpdater:IOmniSharpUpdater = container.get<IOmniSharpUpdater>(TYPES.IOmniSharpUpdater);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            configuration.omniSharpJsonConfigurationContent = 
                // eslint-disable-next-line @typescript-eslint/naming-convention
                {"RoslynExtensionsOptions": {
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    "EnableAnalyzersSupport": true,
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        "LocationPaths": [
                        ]}
                };                
            const analyzerPath = 'myTestPath';

            // ACT
            omniSharpUpdater.update(analyzerPath, false);

            // ASSERT
            chai.assert.strictEqual(configuration.omniSharpJsonConfigurationContent.RoslynExtensionsOptions.LocationPaths[0], analyzerPath);
        });

        it('should update location when update is set to true', function() {
            // ARANGE
            initializeGenericTestsIoC();
            const container = initializeGenericTestsIoC();
            let omniSharpUpdater:IOmniSharpUpdater = container.get<IOmniSharpUpdater>(TYPES.IOmniSharpUpdater);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            configuration.omniSharpJsonConfigurationContent = 
                // eslint-disable-next-line @typescript-eslint/naming-convention
                {"RoslynExtensionsOptions": {
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    "EnableAnalyzersSupport": true,
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        "LocationPaths": [ 'sonaranalyzer.csharp.1.1'
                        ]}
                };                
            const analyzerPath = 'sonaranalyzer.csharp.1.2';

            // ACT
            omniSharpUpdater.update(analyzerPath, true);

            // ASSERT
            chai.assert.strictEqual(configuration.omniSharpJsonConfigurationContent.RoslynExtensionsOptions.LocationPaths.length, 1);
            chai.assert.strictEqual(configuration.omniSharpJsonConfigurationContent.RoslynExtensionsOptions.LocationPaths[0], analyzerPath);
        });

        it('should enable enableRoslynAnalyzers and EnableAnalyzersSupport Omnisharp parameters', function() {
            // ARANGE
            initializeGenericTestsIoC();
            const container = initializeGenericTestsIoC();
            let omniSharpUpdater:IOmniSharpUpdater = container.get<IOmniSharpUpdater>(TYPES.IOmniSharpUpdater);
            const configuration = container.get<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration) as TestsConfiguration;
            configuration.enableRoslynAnalyzersOmniSharpConfiguration = false;
            configuration.omniSharpJsonConfigurationContent = 
                // eslint-disable-next-line @typescript-eslint/naming-convention
                {"RoslynExtensionsOptions": {
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    "EnableAnalyzersSupport": false,
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        "LocationPaths": [ 'sonaranalyzer.csharp.1.1'
                        ]}
                };                
            const analyzerPath = 'sonaranalyzer.csharp.1.2';

            // ACT
            omniSharpUpdater.update(analyzerPath, true);

            // ASSERT
            chai.assert.strictEqual(configuration.enableRoslynAnalyzersOmniSharpConfiguration, true);
            chai.assert.strictEqual(configuration.omniSharpJsonConfigurationContent.RoslynExtensionsOptions.EnableAnalyzersSupport, true);
        });
    });
});

function initializeGenericTestsIoC():Container {
    const result = new Container();
    result.bind<ILogger>(TYPES.ILogger).to(NullLogger);
    result.bind<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration).to(TestsConfiguration).inSingletonScope();
    result.bind<IOmniSharpUpdater>(TYPES.IOmniSharpUpdater).to(OmniSharpUpdater).inSingletonScope();
    return result;
  }