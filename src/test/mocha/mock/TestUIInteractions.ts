import { injectable } from "inversify";
import { IUIInteractions } from "../../../utils/IUIInteractions";

@injectable()
export class TestUIInteractions implements IUIInteractions{
  informationCallBack:((message:string)=>void)|undefined;
  warningCallBack:((message:string)=>void)|undefined;
  errorCallBack:((message:string)=>void)|undefined;

  showErrorMessage(message: string) {
    if(this.errorCallBack){
      this.errorCallBack(message);
    }      
  }
  showWarningMessage(message: string) {
    if(this.warningCallBack){
      this.warningCallBack(message);
    }
  }
  showInformationMessage(message: string) {
    if(this.informationCallBack){
      this.informationCallBack(message);
    }
  }
}