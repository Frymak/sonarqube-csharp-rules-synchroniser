import { injectable } from "inversify";
import { ILogger } from "../../../utils/ILogger";
import "reflect-metadata";

@injectable()
export class NullLogger implements ILogger{
  append(value: string): void {
  }
  appendLine(value: string): void {
  }
  clear(): void {
  }
}