import { injectable } from "inversify";
import { IVsCodeConfiguration } from "../../../utils/IVsCodeConfiguration";

@injectable()
export class TestsConfiguration implements IVsCodeConfiguration{
  rulesetRelativepath:string|undefined;
  sonarQubeProjectkey:string|undefined;
  sonarQubeToken:string|undefined;
  sonarQubeRootUrl:string|undefined;
  workspaceRootFolderUri:string = '';
  sonarlintCompatibilityMode:boolean = true;
  enableRoslynAnalyzersOmniSharpConfiguration:boolean = false;
  omniSharpJsonConfigurationContent:any = undefined;
  nugetOrgRootUrl:string = "https://www.nuget.org";
  getOrCreateOmniSharpJsonConfigurationContent() {
    return this.omniSharpJsonConfigurationContent;
  }
  updateOmniSharpJsonConfigurationContent(content: string, callback: (omnisharpJsonFilepath: string) => void): void {
    this.omniSharpJsonConfigurationContent = content;
    callback('omnisharpDummyTestPath');
  }

  getEnableRoslynAnalyzersOmniSharpConfiguration(): boolean | undefined {
    return this.enableRoslynAnalyzersOmniSharpConfiguration;
  }
  updateEnableRoslynAnalyzersOmniSharpConfiguration(value: boolean, globalConfigurationTarget: boolean | undefined): void {
    this.enableRoslynAnalyzersOmniSharpConfiguration = value;
  }
  getWorkspaceRootFolderUri(): string {
    return this.workspaceRootFolderUri;
  }
  getSonarQubeRootUrl(): string | undefined {
    return this.sonarQubeRootUrl;
  }
  getSonarQubeToken(): string | undefined {
    return this.sonarQubeToken;
  }
  getSonarQubeProjectkey(): string | undefined {
    return this.sonarQubeProjectkey;
  }
  getRulesetRelativepath(): string | undefined {
    return this.rulesetRelativepath;
  }
  getSonarlintCompatibilityMode(): boolean {
    return this.sonarlintCompatibilityMode;
  }
  getNugetOrgRootUrl(): string{
    return this.nugetOrgRootUrl;
  }
}