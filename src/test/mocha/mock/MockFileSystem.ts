import { MakeDirectoryOptions, NoParamCallback, PathLike, ReadStream } from "fs";
import { injectable } from "inversify";
import { IFileSystem } from "../../../utils/IFileSystem";

@injectable()
export class MockFileSystem implements IFileSystem{
    public writtenFiles:{[path:string]: string} = {};
    public createdDirectories:string[] = [];
    public deletedItems:string[] = [];
    public existsFunc:(path:string|void)=>boolean = () => true;
    public isDirectoryFunc:(path:PathLike|void)=>boolean = () => true;
    public readFileFunc:(path:string)=>Buffer = () => Buffer.from('');
    public mockedDirectories:{[path:string]: string[]} = {};
    realpathSync(path: PathLike, options?: { encoding?: BufferEncoding | null } | BufferEncoding | null): string {
        return path.toString();
    }
    writeFileSync(path: string, content: any): void {
        this.writtenFiles[path] = content;
    }
    writeFile(path: PathLike | number, data: any, callback: NoParamCallback): void{
        this.writtenFiles[path.toString()] = data;
    }
    isDirectory(path: PathLike): boolean{
        return this.isDirectoryFunc(path);
    }
    readdirSync(path: PathLike, options?: { encoding: BufferEncoding | null; withFileTypes?: false } | BufferEncoding | null): string[]{
        return this.mockedDirectories[path.toString()] ?? [];
    }
    readFileSync(path: PathLike | number, options?: { encoding?: null; flag?: string; } | null): Buffer{
        return this.readFileFunc(path.toString());}
    existsSync(path: PathLike):boolean{
        return this.existsFunc(path.toString());}
    mkdirSync(path: PathLike, options?: number | string | MakeDirectoryOptions | null): void{
        this.createdDirectories.push(path.toString());
    }
    unlinkSync(path: PathLike): void {
        this.deletedItems.push(path.toString());
    }
    unlink(path: PathLike, callback: NoParamCallback): void {
        this.deletedItems.push(path.toString());
    }
    createReadStream(path: PathLike): ReadStream {
        return ReadStream.from(this.writtenFiles[path.toString()] ?? '') as ReadStream;
    }
    removeRecursiveForced(path: string, callback: (error: Error) => void): void {
        this.deletedItems.push(path);
    }
}