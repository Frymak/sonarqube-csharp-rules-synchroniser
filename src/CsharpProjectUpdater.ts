import * as path from 'path';
import { inject, injectable } from 'inversify';
import "reflect-metadata";
import TYPES from './Types';
import { DOMParser, XMLSerializer } from 'xmldom';
import { IFileSystem } from './utils/IFileSystem';
import { ILogger } from "./utils/ILogger";
import { IVsCodeConfiguration } from "./utils/IVsCodeConfiguration";
import { IUIInteractions } from "./utils/IUIInteractions";
import { IRulesetFileIO } from "./interfaces/IRulesetFileIO";

enum TypeOfSearch{
    byFilename ,
    bySuffix
}
@injectable()
export class CsharpProjectUpdater implements IProjectUpdater{
    private _logger:ILogger;
    private _fileSystem: IFileSystem;
    private _configuration:IVsCodeConfiguration;
    private _interactions:IUIInteractions;
    private _rulesetFileIO:IRulesetFileIO;

    constructor(
        @inject(TYPES.IFileSystem) fileSystem: IFileSystem,
        @inject(TYPES.ILogger)logger: ILogger,
        @inject(TYPES.IVsCodeConfiguration)configuration: IVsCodeConfiguration,
        @inject(TYPES.IUIInteractions)interactions:IUIInteractions,
        @inject(TYPES.IRulesetFileIO)rulesetFileIO:IRulesetFileIO){
        this._logger = logger;
        this._configuration=configuration;
        this._fileSystem = fileSystem;
        this._interactions = interactions;
        this._rulesetFileIO = rulesetFileIO;
    }

    update(){
        const sonarLintCompatibilityMode = this._configuration.getSonarlintCompatibilityMode();
        const shouldRestart = ()=>{this._interactions.showWarningMessage('VSCode has to be restarted to load C# ruleset correctly.');};
        const rootFolder = this._configuration.getWorkspaceRootFolderUri();
        let files:string[];
        if(sonarLintCompatibilityMode){
            files = this.findFileRecusrively(rootFolder, '.csproj', TypeOfSearch.bySuffix);
        }
        else{
            files = this.findFileRecusrively(rootFolder, 'Directory.Build.props', TypeOfSearch.byFilename);
        }
        
        const sonarqubeRulesSynchronizer = "sonarqube-rules-synchronizer";
        if(files.length > 0){
            let restartNeeded:boolean = false;
            files.forEach((projFile:string)=>{
                if(!(sonarLintCompatibilityMode && projFile.toLowerCase().includes("test"))){
                    const rulesetPathToSet = this._rulesetFileIO.getConfiguredPath(projFile);
                    
                    // the label on the following xml node is not the current implementation of sonarlint,
                    // but sonarlint doesn't matter about this Label, so we can keep it...
                    let targetContent = '  <PropertyGroup Label="' + sonarqubeRulesSynchronizer + '">\n'; 
                    targetContent += '    <CodeAnalysisRuleSet>' + rulesetPathToSet + '</CodeAnalysisRuleSet>\n';
                    targetContent += '  </PropertyGroup>\n';
                    // targetContent += '</Project>';
                    
                    const fileContent = this._fileSystem.readFileSync(projFile).toString();

                    const xmlParser:DOMParser = new DOMParser();
                    const xmlContent = xmlParser.parseFromString(fileContent);
                    try{
                        let rulesetAlreadySet = false;
                        Array.prototype.forEach.call(xmlContent.documentElement.childNodes, (value:Element, key, parent) => {
                            if(value.nodeName === 'PropertyGroup'){
                                Array.prototype.forEach.call(value.childNodes, (v, k, p) => {
                                    if(v.nodeName === "CodeAnalysisRuleSet") {
                                        if(v.textContent === rulesetPathToSet){
                                            // nothing todo, the ruleset is correctly set in the project
                                            rulesetAlreadySet = true;
                                        }
                                        else{
                                            // check if the node was created by this plugin and if so remove the node to add it later with the updated ruleset path.
                                            if(value.attributes.getNamedItem('Label')?.textContent === sonarqubeRulesSynchronizer){
                                                xmlContent.documentElement.removeChild(value);
                                            }
                                        }
                                    }
                                });
                            }
                        });

                        if(!rulesetAlreadySet){
                            xmlContent.documentElement.appendChild(xmlParser.parseFromString(targetContent));
                            const serializer:XMLSerializer = new XMLSerializer();
                            const serialized = serializer.serializeToString(xmlContent);
                            this._fileSystem.writeFile(projFile, serialized, _ => this._logger.appendLine("Project " + projFile + " updated."));
                            restartNeeded = true;
                        }
                    }
                    catch(ex){
                        console.error(ex);
                        this._logger.appendLine(ex);
                    }
                }
            });
            if(restartNeeded){
                shouldRestart();
            }
        }
        else{
            // if we go in this else condition part, we found 0 project file, so we are not on csproj mode for sure but on directory.build.props file. 
            // <=> so we're not in sonarlint compatibility mode

            let targetContent ='<Project>\n';
            // the label on the following xml node is not the current implementation of sonarlint,
            // but sonarlint doesn't matter about this Label, so we can keep it...
            targetContent += '  <PropertyGroup Label="' + sonarqubeRulesSynchronizer + '">\n'; 
            targetContent += '    <CodeAnalysisRuleSet>' + this._rulesetFileIO.getConfiguredPath('') + '</CodeAnalysisRuleSet>\n';
            targetContent += '  </PropertyGroup>\n';
            targetContent += '</Project>';
            const rootPropsFilePath = rootFolder + '/Directory.Build.props';
            this._fileSystem.writeFile(rootPropsFilePath, targetContent, ()=> this._logger.appendLine('File updated: ' + rootPropsFilePath));
            shouldRestart();
        }        
    }

    private findFileRecusrively(folder:string,search:string,searchType:TypeOfSearch, files:string[]|undefined=undefined,result:string[]|undefined=undefined):string[]
    {
        files = files || this._fileSystem.readdirSync(folder);
        if(result === undefined){
            result=[];
        }

        files.forEach((file) => {
                if(!['.git','.vs', '.vscode', 'bin', 'obj', 'packages'].includes(file)){
                    var newItem = path.join(folder,file);
                    if (this._fileSystem.isDirectory(newItem)){
                        result = this.findFileRecusrively(newItem,search,searchType,this._fileSystem.readdirSync(newItem),result);
                    }
                    else
                    {
                        const searchByFileName = searchType === TypeOfSearch.byFilename;
                        const fileToLower = file.toLowerCase();
                        const searchToLower = search.toLowerCase();
                        if (searchByFileName ? fileToLower === searchToLower : fileToLower.endsWith(searchToLower)){
                                result!.push(newItem);
                        } 
                    }
                }
            }
        );

        return result;
    }    
}