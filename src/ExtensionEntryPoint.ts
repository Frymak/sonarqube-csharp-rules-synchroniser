import { inject, injectable } from 'inversify';
import TYPES from './Types';
import { IExtensionEntryPoint } from './interfaces/IExtensionEntryPoint';
import "reflect-metadata";
import { IVsCodeConfiguration } from './utils/IVsCodeConfiguration';
import { ILogger } from './utils/ILogger';
import { IUIInteractions } from './utils/IUIInteractions';
import { ISonarQubeClient } from './interfaces/ISonarQubeClient';
import { IOmniSharpUpdater } from './interfaces/IOmniSharpUpdater';
import { IAnalyzersInstaller } from './interfaces/IAnalyzersInstaller';

@injectable()
export class ExtensionEntryPoint implements IExtensionEntryPoint {
    private _projectUpdater: IProjectUpdater;
    private _logger: ILogger;
    private _configuration: IVsCodeConfiguration;
    private _interactions: IUIInteractions;
    private _sonarQubeClient:ISonarQubeClient;
    private _omniSharpUpdater:IOmniSharpUpdater;
    private _analyzersInstaller:IAnalyzersInstaller;

    constructor(@inject(TYPES.IProjectUpdater)projectUpdater:IProjectUpdater,
                @inject(TYPES.ILogger)logger:ILogger,
                @inject(TYPES.IVsCodeConfiguration)configuration:IVsCodeConfiguration,
                @inject(TYPES.IUIInteractions)interactions:IUIInteractions,
                @inject(TYPES.ISonarQubeClient)sonarQubeClient:ISonarQubeClient,
                @inject(TYPES.IOmniSharpUpdater)omniSharpUpdater:IOmniSharpUpdater,
                @inject(TYPES.IAnalyzersInstaller)analyzersInstaller:IAnalyzersInstaller){
        this._projectUpdater = projectUpdater;
        this._logger = logger;
        this._configuration = configuration;
        this._interactions = interactions;
        this._sonarQubeClient = sonarQubeClient;
        this._omniSharpUpdater =  omniSharpUpdater;
        this._analyzersInstaller = analyzersInstaller;
    }

    /**
 * This is the synchronize command code entry point of this extension.
 * It synchronizes the SonarQube ruleset locally by calling SonarQube Api for bounded project and retrieve the corresponding quality profile and then the rules set.
 */
    synchronizeCommand(){
        try{
            // The code you place here will be executed every time your command is executed
            const sonarQubeRootUrl:string | undefined = this._configuration.getSonarQubeRootUrl();
            const sonarQubeToken:string | undefined = this._configuration.getSonarQubeToken();
            const sonarQubeProjectkey:string | undefined = this._configuration.getSonarQubeProjectkey();
            if(!sonarQubeRootUrl || !sonarQubeToken || !sonarQubeProjectkey){
                this._logger.appendLine("Error: Configuration paramters missing, please review the configuration");
                this._interactions.showErrorMessage('SonarQube Rules Synchronization will stop, Configuration paramters missing, please review the configuration.');
                return;
            }

            this._interactions.showInformationMessage('Starting SonarQube Rules Synchronization...');

            this._sonarQubeClient.synchronizeQualityRules(sonarQubeProjectkey, () => {
                this._projectUpdater.update();
            });	
        }	
        catch(ex){
            this._logger.appendLine("ERROR during synchronizing ruleset:");
            this._logger.appendLine("-> "+ ex);
        }
    }

    installOrUpdateAnalyzersCommand():void{
        try{
            this._analyzersInstaller.cleanAtStart();
            this._analyzersInstaller.install(
                // the following callback is always called, even if nothing has been done by analyzersInstaller. 
                // It is always done to be able to ensure omnisharp configuration is OK.
                (analyserInstalled:boolean|undefined, analyzersPath:string, isUpdating:boolean) => {
                    // we just ensure analyserInstalled is true or false because if undefined, it means an error raised and in this case we do nothing...
                    if(analyserInstalled !== undefined) { 
                        this._omniSharpUpdater.update(analyzersPath, isUpdating);
                    }
                },
                // last param 'updateIfAlreadyInstalled' is set to true to auto-update sonar.analyzers 
                // if already installed to always run with latest version
                true);
        }
        catch(ex){
            this._logger.appendLine("ERROR during installing or updating analyzers:");
            this._logger.appendLine("-> "+ ex);
        }
    }
}