// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { OmniSharpUpdater } from './OmniSharpUpdater';
import { SonarRoslynAnalyzersInstaller } from './SonarRoslynAnalyzersInstaller';
import TYPES from './Types';
import { IExtensionEntryPoint } from './interfaces/IExtensionEntryPoint';
import { Container } from 'inversify';
import { IFileSystem } from './utils/IFileSystem';
import { CsharpProjectUpdater } from './CsharpProjectUpdater';
import { ExtensionEntryPoint } from './ExtensionEntryPoint';
import { FileSystem } from './utils/FileSystem';
import { IVsCodeConfiguration } from './utils/IVsCodeConfiguration';
import { VsCodeConfiguration } from './utils/VsCodeConfiguration';
import { VsCodeUIInteractions } from './utils/VsCodeUIInteractions';
import { IUIInteractions } from './utils/IUIInteractions';
import { OutputChannelLogger } from './utils/OutputChannelLogger';
import { ILogger } from './utils/ILogger';
import { ISonarQubeClient } from './interfaces/ISonarQubeClient';
import { SonarQubeClient } from './SonarQubeClient';
import { IOmniSharpUpdater } from './interfaces/IOmniSharpUpdater';
import { IAnalyzersInstaller } from './interfaces/IAnalyzersInstaller';
import { IRulesetFileIO } from './interfaces/IRulesetFileIO';
import { RulesetFileIO } from './utils/RulesetFileIO';
import { IUnzipper } from './utils/IUnzipper';
import { Unzipper } from './utils/Unzipper';

let logger:ILogger;
const homedir = require('os').homedir();
let container:Container;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	container = initializeIoC();
    logger = container.get<ILogger>(TYPES.ILogger);
	const configuration = vscode.workspace.getConfiguration('sonarqube-rules-synchroniser');
	
	// The command has been defined in the package.json file
	// The commandId parameter must match the command field in package.json
	const disposable = vscode.commands.registerCommand('sonarqube-rules-synchroniser.synchronizecsharp', synchronizeCommand);
	context.subscriptions.push(disposable);

	
	if(configuration.get('autosynchronize-on-startup')){
		logger.appendLine('Automatic synchronization is enabled.');
		synchronizeCommand();
	}
	
	if(configuration.get('autoconfigure-csharp-analyzers')){
		const entryPoint:IExtensionEntryPoint = container.get<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint);
		entryPoint.installOrUpdateAnalyzersCommand();
	}
}

function initializeIoC():Container {
	const result = new Container();
	result.bind<IFileSystem>(TYPES.IFileSystem).to(FileSystem).inSingletonScope();
	result.bind<IProjectUpdater>(TYPES.IProjectUpdater).to(CsharpProjectUpdater).inSingletonScope();
	result.bind<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint).to(ExtensionEntryPoint).inSingletonScope();
	result.bind<IVsCodeConfiguration>(TYPES.IVsCodeConfiguration).to(VsCodeConfiguration).inSingletonScope();
	result.bind<IUIInteractions>(TYPES.IUIInteractions).to(VsCodeUIInteractions).inSingletonScope();
	result.bind<ISonarQubeClient>(TYPES.ISonarQubeClient).to(SonarQubeClient).inSingletonScope();
	result.bind<IOmniSharpUpdater>(TYPES.IOmniSharpUpdater).to(OmniSharpUpdater).inSingletonScope();
	result.bind<IAnalyzersInstaller>(TYPES.IAnalyzersInstaller).to(SonarRoslynAnalyzersInstaller).inSingletonScope();
	result.bind<IRulesetFileIO>(TYPES.IRulesetFileIO).to(RulesetFileIO).inSingletonScope();
	result.bind<IUnzipper>(TYPES.IUnzipper).to(Unzipper).inSingletonScope();
	result.bind<ILogger>(TYPES.ILogger).to(OutputChannelLogger).inSingletonScope();
	return result;
}

// this method is called when your extension is deactivated
export function deactivate() {}

function synchronizeCommand(){
	const entryPoint:IExtensionEntryPoint = container.get<IExtensionEntryPoint>(TYPES.IExtensionEntryPoint);
	entryPoint.synchronizeCommand();
}