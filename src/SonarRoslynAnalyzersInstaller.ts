import * as http from 'http';
import * as https from 'https';
import * as semver from 'semver';
import { ILogger } from './utils/ILogger';
import { inject, injectable } from 'inversify';
import TYPES from './Types';
import { IAnalyzersInstaller } from './interfaces/IAnalyzersInstaller';
import { IUIInteractions } from './utils/IUIInteractions';
import { IFileSystem } from './utils/IFileSystem';
import { Constants } from './Constants';
import { IUnzipper } from './utils/IUnzipper';
import { IVsCodeConfiguration } from './utils/IVsCodeConfiguration';

const homedir = require('os').homedir();

@injectable()
export class SonarRoslynAnalyzersInstaller implements IAnalyzersInstaller{
    private _logger:ILogger;
    private _interactions:IUIInteractions;
    private _fileSystem:IFileSystem;
    private _unzipper:IUnzipper;
    private _configuration:IVsCodeConfiguration;

    constructor(
        @inject(TYPES.ILogger)logger:ILogger,
        @inject(TYPES.IUIInteractions)interactions:IUIInteractions,
        @inject(TYPES.IFileSystem)fileSystem:IFileSystem,
        @inject(TYPES.IUnzipper)unzipper:IUnzipper,
        @inject(TYPES.IVsCodeConfiguration)configuration:IVsCodeConfiguration){
        this._logger = logger;
        this._interactions = interactions;
        this._fileSystem = fileSystem;
        this._unzipper = unzipper;
        this._configuration = configuration;
    }

    /**
    * This method installs the SonarQube analyser by retrieving the SonarAnalyzer.csharp nuget package and extracting it locally on the computer...
    * @param callback: allows to be called-back with the path to the installed analyzers.
    */
    install(callback:(analyserInstalled:boolean|undefined, analyzersPath:string, isUpdating:boolean)=>void, updateIfAlreadyInstalled:boolean=false){
        const extensionUserFolder = homedir + Constants.pluginPath;
        const extensionUserFolderHasBeenCreated:boolean = this.createFolderIfNeeded(extensionUserFolder);
        const ls = this._fileSystem.readdirSync(extensionUserFolder);
        if(extensionUserFolderHasBeenCreated || !ls.some((entry) => entry.toLowerCase().includes('sonaranalyzer.csharp'))){
            this.getSonarAnalyzers(extensionUserFolder, callback);
       }
       else{
            if(updateIfAlreadyInstalled){
                this.getSonarAnalyzers(extensionUserFolder, callback, true);
            }
            else{
                // here, in any case, we still want to ensure we call the callback method
                const analyzersCurrentFullPath = extensionUserFolder + this.getAnalyzersFolderRelativePath(extensionUserFolder) + Constants.analysersPathLastSegment;
                callback(false, analyzersCurrentFullPath, false);
            }
       }
    }

    cleanAtStart(){
        const extensionUserFolder = homedir + Constants.pluginPath;
        const toDeleteFile = extensionUserFolder + '/.todelete';
        if(this._fileSystem.existsSync(toDeleteFile)){
            const path = this._fileSystem.readFileSync(toDeleteFile).toString();
            if(path.length > 0 && this._fileSystem.existsSync(path)){
                this._fileSystem.removeRecursiveForced(path, () => this._logger.appendLine('Previous analyzers version removed: ' + path));
            }
            this._fileSystem.unlinkSync(toDeleteFile);
        }

        // sometimes for unknown reason, analysers folders are still there even with the todelete mechanism... so I try to detect unused folder and delete them here
        this.cleanNonUsedCsharpAnalyzerFolders();
    }

    private cleanNonUsedCsharpAnalyzerFolders():void{
        try{
            const foldersToRemove:string[] = [];
            const omnisharpJson = this._configuration.getOrCreateOmniSharpJsonConfigurationContent();
            const locations:string[] = omnisharpJson.RoslynExtensionsOptions.LocationPaths;
            const analyzerFolders:string[] = this._fileSystem.readdirSync(homedir + Constants.pluginPath);
            
            analyzerFolders.forEach(folder => {
                let toRemove = true;
                locations.forEach(l => {
                    if(l.includes(folder)){
                        toRemove = false;
                    }
                });

                if(toRemove){
                    foldersToRemove.push(folder);
                }
            });

            foldersToRemove.forEach(f => this._fileSystem.removeRecursiveForced(homedir + Constants.pluginPath + f, () => this._logger.appendLine('Previous unused analyzers version removed: ' + f)));
        }
        catch(error){
            this._logger.appendLine("ERROR during cleaning non used Csharp Analyzer folder: " + error);
        }
	}

    private getSonarAnalyzers(extensionUserFolder:string, callback:(analyserInstalled:boolean|undefined, analyzersPath:string, isUpdating:boolean)=>void, update:boolean=false){
        // calling 'https://www.nuget.org/api/v2/package/SonarAnalyzer.CSharp/' will result in a 302 redirect to the latest version...
        //this.getWithFollowRedirect("https://www.nuget.org" + "/api/v2/package/SonarAnalyzer.CSharp/", (finalUri, content)=>{
        this.getWithFollowRedirect(this._configuration.getNugetOrgRootUrl() + "/api/v2/package/SonarAnalyzer.CSharp/", (finalUri, content)=>{
            let updateTodo = false;    
            let currentAnalyzersFolder:string|undefined;
            if(update){
                currentAnalyzersFolder = this.getAnalyzersFolderRelativePath(extensionUserFolder);
                if(currentAnalyzersFolder === undefined){
                    updateTodo = true;
                }
                else{
                    updateTodo = this.checkUpdate(finalUri, currentAnalyzersFolder);
                }
            }

            if(!update || updateTodo){
                let analyzerExtractedFolder = this.saveAnalyzers(finalUri, extensionUserFolder, content, updateTodo, currentAnalyzersFolder);
                let analyzersPath = analyzerExtractedFolder + Constants.analysersPathLastSegment;
                while(analyzersPath.includes('\\')){
                    analyzersPath = analyzersPath.replace('\\','/');
                }

                callback(true, analyzersPath, updateTodo);
            }
            else{
                const analyzersCurrentFullPath = extensionUserFolder + currentAnalyzersFolder + Constants.analysersPathLastSegment;
                // here, we still to the callback even if no update was done because later, we update the omnisharp configuration from this folder, 
                // so we just want to ensure configuration is done in any case.
                callback(false, analyzersCurrentFullPath, updateTodo);
            }
        },
        (err:any) => {
            this._logger.appendLine('Error: ' + err.message);
            this._interactions.showErrorMessage("Error: " + err.message);
            callback(undefined, '', false);
        });
    }

    private getAnalyzersFolderRelativePath(extensionUserFolder:string):string|undefined{
        return this._fileSystem.readdirSync(extensionUserFolder).find((e)=>e.includes('sonaranalyzer.csharp'));
    }

    private checkUpdate(finalUri:string, currentFolder:string):boolean{
        let onlineVersionString:string = finalUri.replace('sonaranalyzer.csharp.', '');
        onlineVersionString = onlineVersionString.replace('.nupkg','');
        onlineVersionString = onlineVersionString.substring(onlineVersionString.lastIndexOf('/')+1);
        const currentVersionString:string = currentFolder.replace('sonaranalyzer.csharp.', '');
        return semver.gt(semver.coerce(onlineVersionString)??'', semver.coerce(currentVersionString)??'');
    }

    private saveAnalyzers(finalUri:string, extensionUserFolder:string, content:Buffer, isUpdating:boolean, oldAnalyzersFolder:string|undefined):string{
        const endOfString = finalUri.lastIndexOf('.nupkg');
        const startOfString = finalUri.lastIndexOf('/', endOfString);
        const filename = extensionUserFolder + finalUri.substring(startOfString+1, endOfString);
        if(!this._fileSystem.existsSync(filename)){
            const filenameWithExt = extensionUserFolder + finalUri.substring(startOfString+1, endOfString) + '.nupkg';
            this._logger.appendLine('-> Writing temporary file: ' + filenameWithExt);
            this._fileSystem.writeFileSync(filenameWithExt, content);
            this._logger.appendLine('-> Extracting temporary file to: ' + filename);
            this._fileSystem.createReadStream(filenameWithExt)
                .pipe(this._unzipper.extract({ path: filename }))
                .on('close', () => {
                    this._fileSystem.unlink(filenameWithExt, () => this._logger.appendLine('Temporary file "' + filenameWithExt + '" deleted.'));
                    this._interactions.showInformationMessage('Sonar C# analyzers installed to: ' + filename);
                });
                this._logger.appendLine('Analyzers extracted: ' + filename);
                if(isUpdating){
                    if(oldAnalyzersFolder){
                        const fullPath = extensionUserFolder + '/' + oldAnalyzersFolder;
                        this._fileSystem.writeFile(extensionUserFolder + '/.todelete', fullPath, ()=>{ this._logger.appendLine('Previous analyzers will be removed at next vscode start: ' + fullPath);});
                    }
                }
        }
        else{
            this._logger.appendLine('Analyzers already exist at: ' + filename);
        }
        return filename;
    }

    private createFolderIfNeeded(extensionUserFolder:string):boolean{        
        let rootHasBeenCreated = false;
        if(!this._fileSystem.existsSync(extensionUserFolder)){
            this._fileSystem.mkdirSync(extensionUserFolder);
            rootHasBeenCreated = true;
        }
        return rootHasBeenCreated;
    }

    /**
     * This get method is just a wrapper around http.get/https.get with redirect 302 auto follow.
     * @param uri is the uri to call.
     * @param manageContent is a callback to do whatever you want with the retrieved content as a buffer. The finalUri is the last uri followed if any redirect.
     * @param errorFunc is a callback function to call if any error is encountered
     * @param i is the number of followed redirections
     */
    private getWithFollowRedirect(uri:string, manageContent:(finalUri:string, requestResult:Buffer)=>void ,errorFunc:(err:any)=>void, i?:number){
        const maxRedirects:number = 10;
        const chunks:any[] = [];
        let httpClient;
        if(uri.startsWith('https')){
            httpClient = https;
        }
        else{
            httpClient = http;
        }
        httpClient.get(uri, res => {
            if(res.statusCode === 302 && res !== undefined && res.headers !== undefined && res.headers.location !== undefined && (i === undefined || i < maxRedirects)){
                this.getWithFollowRedirect(res.headers.location, manageContent,errorFunc, i === undefined ? 2 : i+1);
                return;
            }
            res.on('data', (chunk) => {
                chunks.push(chunk);
                this._logger.append('.');
            });
            res.on('error', errorFunc); 
            res.on('end', () => {
                manageContent(uri, Buffer.concat(chunks));
            });
        }).on('error', errorFunc);
    }
}
