export class Constants{
    public static readonly pluginPath = '/.sonarqube-rules-synchronizer/';
    public static readonly analysersPathLastSegment:string = '/analyzers';
}