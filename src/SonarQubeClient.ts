import * as http from 'http';
import * as https from 'https';
import { Base64 } from 'js-base64';
import { inject, injectable } from 'inversify';
import TYPES from './Types';
import { ILogger } from './utils/ILogger';
import { VsCodeUIInteractions } from './utils/VsCodeUIInteractions';
import { IUIInteractions } from './utils/IUIInteractions';
import { IVsCodeConfiguration } from './utils/IVsCodeConfiguration';
import { ISonarQubeClient } from './interfaces/ISonarQubeClient';
import { IRulesetFileIO } from './interfaces/IRulesetFileIO';

@injectable()
export class SonarQubeClient implements ISonarQubeClient{
    private _logger:ILogger;
    private _sonarQubeRootUrl:string;
    private _sonarQubeToken:string;
    private _interactions:VsCodeUIInteractions;
    private _rulesetFileIO:IRulesetFileIO;

    constructor(@inject(TYPES.ILogger)logger:ILogger, 
                @inject(TYPES.IUIInteractions)interactions:IUIInteractions,
                @inject(TYPES.IVsCodeConfiguration)configuration:IVsCodeConfiguration, 
                @inject(TYPES.IRulesetFileIO)rulesetFileIO:IRulesetFileIO){
        this._logger=logger;
        this._sonarQubeRootUrl=configuration.getSonarQubeRootUrl()??'';
        this._sonarQubeToken=configuration.getSonarQubeToken()??'';
        this._interactions=interactions;
        this._rulesetFileIO = rulesetFileIO;
        // we'll use https to connect to sonarqube here, so we use the SSL validation for now (as in many contexts, it may be a company self-signed certificate...)
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    }

    synchronizeQualityRules(sonarQubeProjectkey:string, callback:()=>void) {
        let qualityProfileHttpContent = '';
        const requestOptions = {
            headers: {
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'Content-Type': 'application/json',
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'Accept': 'application/json',
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'Access-Control-Allow-Headers': 'Content-Type',
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'Authorization': 'Basic ' + Base64.btoa(this._sonarQubeToken.trim() + ':')
            }
        };
    
        if (!this._sonarQubeRootUrl.endsWith('/')) {
            this._sonarQubeRootUrl = this._sonarQubeRootUrl + '/';
        }
        
        const errorFunc = (err:any) => {
            this._logger.appendLine('Error: ' + err.message);
            this._interactions.showErrorMessage("Error: " + err.message);
        };
        
        this._logger.appendLine('Synchronizer will now try to get quality profile name for project ' + sonarQubeProjectkey);
        const uri = this._sonarQubeRootUrl + "api/qualityprofiles/search?format=json&language=cs&project=" + sonarQubeProjectkey;
        this._logger.appendLine('-> Calling: ' + uri);
        let httpClient;
        if(uri.startsWith('https')){
            httpClient = https;
        }
        else{
            httpClient = http;
        }
        httpClient.get(uri, requestOptions, res => {
            if (!res.headers['content-type']?.includes('application/json')) {
                this._interactions.showErrorMessage("Error calling SonarQube (api/qualityprofiles/search), API response type is not json, please check your configuration.");
                return;
            }
            res.on('data', (chunk) => {
                qualityProfileHttpContent += chunk;
                this._logger.append('.');
            });
            res.on('error', errorFunc); 
            res.on('end', () => {
                this._logger.appendLine('');
                const json = JSON.parse(qualityProfileHttpContent);
                const qualityProfileName:string = json.profiles[0].name;
                this._logger.appendLine('Synchronizer will now try to get ' + qualityProfileName + ' quality profile.');
                this.getQualityProfileRuleset(qualityProfileName, requestOptions, errorFunc, callback);
            });
        }).on('error', errorFunc);
    }
    
    /**
     * Gets the quality profile rules set.
     * @param sonarQubeRootUrl 
     * @param sonarQubeToken 
     * @param qualityProfileName 
     * @param requestOptions 
     * @param errorFunc 
     * @param rulesetRelativepath 
     */
    private getQualityProfileRuleset(qualityProfileName:string, requestOptions:http.RequestOptions, errorFunc:(err:any)=>void, callback:()=>void) {
        let rulesetContent = '';
        const uri:string = this._sonarQubeRootUrl + "api/qualityprofiles/export?language=cs&exporterKey=sonarlint-vs-cs&qualityProfile=" + qualityProfileName;
        this._logger.appendLine('-> Calling: ' + uri);
        let httpClient;
        if(uri.startsWith('https')){
            httpClient = https;
        }
        else{
            httpClient = http;
        }
        httpClient.get(uri, requestOptions, res => {
            res.on('data', (chunk) => {
                rulesetContent += chunk;
                this._logger.append('.');
            });
            res.on('error', errorFunc); 
            res.on('end', () => {
                this._interactions.showInformationMessage('Ruleset downloaded.');
                this._rulesetFileIO.write(qualityProfileName, rulesetContent);
                callback();
            });
        }).on('error', errorFunc);
    }
}