import { Duplex } from "stream";
import { ParseOptions } from "unzipper";

export interface IUnzipper{
    extract(opts?: ParseOptions): Duplex;
}