import { injectable } from "inversify";
import "reflect-metadata";
import * as vscode from "vscode";
import { ILogger } from "./ILogger";

@injectable()
export class OutputChannelLogger implements ILogger{
    output:vscode.OutputChannel;
    constructor(){
        this.output = vscode.window.createOutputChannel("sonarqube-rules-synchroniser");
    }

    append(value:string):void{
        return this.output.append(value);
    }

    appendLine(value:string):void{
        return this.output.appendLine(value);
    }

    clear():void{
        return this.output.clear();
    }
}