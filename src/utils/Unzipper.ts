import { injectable } from 'inversify';
import { Duplex } from 'stream';
import * as unzipper from 'unzipper';
import { ParseOptions } from 'unzipper';
import { IUnzipper } from "./IUnzipper";

@injectable()
export class Unzipper implements IUnzipper{
    extract(opts?: ParseOptions): Duplex{
        return unzipper.Extract(opts);
    }
}