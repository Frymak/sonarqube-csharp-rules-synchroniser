import { MakeDirectoryOptions, NoParamCallback, PathLike, ReadStream } from "fs";

export interface IFileSystem{
    writeFileSync(path:string, content:any):void;
    writeFile(path: PathLike | number, data: any, callback: NoParamCallback): void;
    isDirectory(path: PathLike): boolean;
    readdirSync(path: PathLike, options?: { encoding: BufferEncoding | null; withFileTypes?: false } | BufferEncoding | null): string[];
    readFileSync(path: PathLike | number, options?: { encoding?: null; flag?: string; } | null): Buffer;
    realpathSync(path: PathLike, options?: { encoding?: BufferEncoding | null } | BufferEncoding | null): string;
    existsSync(path: PathLike):boolean;
    mkdirSync(path: PathLike, options?: number | string | MakeDirectoryOptions | null): void;
    unlinkSync(path: PathLike): void;
    unlink(path: PathLike, callback: NoParamCallback): void
    createReadStream(path: PathLike):ReadStream;
    removeRecursiveForced(path: string, callback: (error: Error) => void): void;
}