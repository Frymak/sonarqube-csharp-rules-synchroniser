import * as fs from "fs";
import * as rimraf from 'rimraf';
import { injectable } from "inversify";
import { IFileSystem } from "./IFileSystem";
import "reflect-metadata";

@injectable()
export class FileSystem implements IFileSystem {
    writeFileSync(path: string, content: any): void {
        fs.writeFileSync(path, content);
    }
    writeFile(path: fs.PathLike | number, data: any, callback: fs.NoParamCallback): void{
        fs.writeFile(path, data, callback);
    }
    isDirectory(path: fs.PathLike): boolean{
        return fs.statSync(path).isDirectory();
    }
    readdirSync(path: fs.PathLike, options?: { encoding: BufferEncoding | null; withFileTypes?: false } | BufferEncoding | null): string[]{
        return fs.readdirSync(path, options);
    }

    readFileSync(path: fs.PathLike | number, options?: { encoding?: null; flag?: string; } | null): Buffer{
        return fs.readFileSync(path, options);
    }

    realpathSync(path: fs.PathLike, options?: { encoding?: BufferEncoding | null } | BufferEncoding | null): string{
        return fs.realpathSync(path, options);
    }
    existsSync(path: fs.PathLike): boolean{
        return fs.existsSync(path);
    }

    mkdirSync(path: fs.PathLike, options?: number | string | fs.MakeDirectoryOptions | null): void{
        fs.mkdirSync(path, options);
    }

    unlinkSync(path: fs.PathLike): void{
        fs.unlinkSync(path);
    }

    unlink(path: fs.PathLike, callback: fs.NoParamCallback): void{
        fs.unlink(path, callback);
    }

    createReadStream(path: fs.PathLike):fs.ReadStream{
        return fs.createReadStream(path);
    }
    
    removeRecursiveForced(path: string, callback: (error: Error) => void): void{
        rimraf(path, callback);
    }
}