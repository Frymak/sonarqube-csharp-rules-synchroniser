import { inject, injectable } from "inversify";
import { IRulesetFileIO } from "../interfaces/IRulesetFileIO";
import TYPES from "../Types";
import { IFileSystem } from "./IFileSystem";
import { ILogger } from "./ILogger";
import { IVsCodeConfiguration } from "./IVsCodeConfiguration";
const homedir = require('os').homedir();

@injectable()
export class RulesetFileIO implements IRulesetFileIO{
    private _configuration:IVsCodeConfiguration;
    private _logger:ILogger;
    private _fileSystem: IFileSystem;
    constructor(
        @inject(TYPES.IFileSystem) fileSystem: IFileSystem,
        @inject(TYPES.ILogger)logger: ILogger,
        @inject(TYPES.IVsCodeConfiguration)configuration: IVsCodeConfiguration){
        this._logger = logger;
        this._configuration=configuration;
        this._fileSystem = fileSystem;
    }
    
    getConfiguredPath(projFile:string):string{
        if(this._configuration.getSonarlintCompatibilityMode()){
            const wf = this._fileSystem.realpathSync(this._configuration.getWorkspaceRootFolderUri());
            const pf = this._fileSystem.realpathSync(projFile);       
            let relativePath:string='';     
            for(let i=0; i < this.replaceAll(pf, '\\\\', '/').replace(this.replaceAll(wf, '\\\\', '/'), '').split('/').length - 2;i++){
                relativePath += '../';
            }

            relativePath += this.getRelativePath();
            return relativePath;
        }
        else{
            return '$(MSBuildThisFileDirectory)\\'+ this.getRelativePath();
        }
    } 

    getFullPath(qualityProfileName:string):string{
        const relativePath = this.getRelativePath(qualityProfileName);
        const rootFolder = this._configuration.getWorkspaceRootFolderUri();
        return rootFolder + '/' + relativePath;
    }

    write(qualityProfileName:string, content:string):void{
        const filename = this.getFullPath(qualityProfileName);                
        this._logger.appendLine('');
        this._logger.appendLine('-> Writing ruleset file to: ' + filename);
        this._fileSystem.writeFileSync(filename, content);
    }

    getDefaultFileName():string{
        // this filename formatting respects SonarLint convention.
        return this.escapeIllegalPathChar(this._configuration.getSonarQubeProjectkey()!.toLowerCase()) + 'csharp.ruleset';
    }

    private getRelativePath(qualityProfileName:string = ''):string{
        if(this._configuration.getSonarlintCompatibilityMode()){
            return  '.sonarlint/' + this.getDefaultFileName(); 
        }
        else{
            let rulesetRelativePath = this._configuration.getRulesetRelativepath();
            if(rulesetRelativePath !== undefined && rulesetRelativePath.trim().length > 0){
                return rulesetRelativePath;
            } 
            else{
                return qualityProfileName !== '' ? qualityProfileName + '.ruleset' : this.getDefaultFileName();
            }
        }
    }   

    private replaceAll(content:string, valueToReplace:string, replacement:string):string{
        let regex:RegExp=new RegExp(valueToReplace, 'g');
        return content.replace(regex, replacement);
    }
    
    private escapeIllegalPathChar(filename:string):string{
        // from what is done in sonarlint plugin https://github.com/SonarSource/sonarlint-visualstudio/blob/master/src/Core/Helpers/PathHelper.cs 
        // =>  EscapeFileName and Path.GetInvalidPathChars(), Path.GetInvalidFileNameChars() from https://referencesource.microsoft.com/#mscorlib/system/io/path.cs,46e74e7b5a5f6053,references
        /**
         *  InvalidPathChars =
        {
            '\"', '<', '>', '|', '\0',
            (char)1, (char)2, (char)3, (char)4, (char)5, (char)6, (char)7, (char)8, (char)9, (char)10,
            (char)11, (char)12, (char)13, (char)14, (char)15, (char)16, (char)17, (char)18, (char)19, (char)20,
            (char)21, (char)22, (char)23, (char)24, (char)25, (char)26, (char)27, (char)28, (char)29, (char)30,
            (char)31, ':', '*', '?', '\\', '/'
        }

        SafeChar used to replace in SonarLint for Visual studio is : safeChar = '_';
         */
        let toReplace = "[\"<>|\\\\" + String.fromCharCode(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31) + ":*?/]";
        return this.replaceAll(filename, toReplace, '_');
    }   
}