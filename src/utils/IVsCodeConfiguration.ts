

export interface IVsCodeConfiguration{
    getNugetOrgRootUrl():string;
    getWorkspaceRootFolderUri():string;
    getSonarQubeRootUrl():string | undefined;
    getSonarQubeToken():string | undefined;
    getSonarQubeProjectkey():string | undefined;
    getRulesetRelativepath():string | undefined;
    getSonarlintCompatibilityMode():boolean;
    getEnableRoslynAnalyzersOmniSharpConfiguration():boolean|undefined;
    getOrCreateOmniSharpJsonConfigurationContent():any;
    updateEnableRoslynAnalyzersOmniSharpConfiguration(value:boolean, globalConfigurationTarget:boolean|undefined):void;
    updateOmniSharpJsonConfigurationContent(content:string, callback:(omnisharpJsonFilepath:string)=>void):void;
}