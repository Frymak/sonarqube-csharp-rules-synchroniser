export interface IUIInteractions{
    showInformationMessage(message: string):any;
    showWarningMessage(message: string):any;
    showErrorMessage(message:string):any;
}