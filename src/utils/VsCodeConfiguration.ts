import * as fs from 'fs';
import { injectable } from "inversify";
import * as vscode from "vscode";
import { IVsCodeConfiguration } from "./IVsCodeConfiguration";
const homedir = require('os').homedir();
@injectable()
export class VsCodeConfiguration implements IVsCodeConfiguration{
    private _omnisharpJsonFilepath:string = homedir + "/.omnisharp/omnisharp.json";
    private _configuration:vscode.WorkspaceConfiguration = vscode.workspace.getConfiguration('sonarqube-rules-synchroniser');
    
    getWorkspaceRootFolderUri():string{
        return vscode.workspace.workspaceFolders![0].uri.fsPath;
    }

    getOmniSharpConfiguration():vscode.WorkspaceConfiguration{
        return vscode.workspace.getConfiguration('omnisharp');
    }   

    getOrCreateOmniSharpJsonConfigurationContent():any{
        if(!fs.existsSync(this._omnisharpJsonFilepath)){
            // eslint-disable-next-line @typescript-eslint/naming-convention
            const content:any = {"RoslynExtensionsOptions": {
                // eslint-disable-next-line @typescript-eslint/naming-convention
                "EnableAnalyzersSupport": true,
                    // eslint-disable-next-line @typescript-eslint/naming-convention
                    "LocationPaths": [
                    ]}
            };
            fs.writeFileSync(this._omnisharpJsonFilepath, JSON.stringify(content));
            return content;
        }
		return JSON.parse(fs.readFileSync(this._omnisharpJsonFilepath).toString());
    }

    updateOmniSharpJsonConfigurationContent(content:string, callback:(omnisharpJsonFilepath:string)=>void):void{
        fs.writeFile(this._omnisharpJsonFilepath, JSON.stringify(content), () => callback(this._omnisharpJsonFilepath));
    }
    
    getEnableRoslynAnalyzersOmniSharpConfiguration():boolean|undefined{
        return vscode.workspace.getConfiguration('omnisharp').get<boolean>('enableRoslynAnalyzers');
    }

    /** Updates the EnableRoslynAnalyzers setting from OmniSharp configuration
     * @param value The new value.
     * @param configurationTarget The [configuration target](#ConfigurationTarget) or a boolean value.
     *    - If `true` updates [Global settings](#ConfigurationTarget.Global).
     *    - If `false` updates [Workspace settings](#ConfigurationTarget.Workspace).
     *    - If `undefined` or `null` updates to [Workspace folder settings](#ConfigurationTarget.WorkspaceFolder) if configuration is resource specific,
     *     otherwise to [Workspace settings](#ConfigurationTarget.Workspace).
     * @throws error while updating
     *    - configuration which is not registered.
     *    - window configuration to workspace folder
     *    - configuration to workspace or workspace folder when no workspace is opened.
     *    - configuration to workspace folder when there is no workspace folder settings.
     *    - configuration to workspace folder when [WorkspaceConfiguration](#WorkspaceConfiguration) is not scoped to a resource.
     */
    updateEnableRoslynAnalyzersOmniSharpConfiguration(value:boolean, globalConfigurationTarget:boolean|undefined){
        vscode.workspace.getConfiguration('omnisharp').update('enableRoslynAnalyzers', true);
    }

    private getConfigItem(key:string, optionalParam:boolean=false):string|undefined{
        const configItem:string | undefined = this._configuration.get(key);
        if(!this._configuration || (!configItem && !optionalParam)){
            vscode.window.showErrorMessage('Configuration '+ key + ' is not correctly set, please review it.');
            return;
        }
        return configItem;
    }

    private getConfigItemBoolean(key:string):boolean|undefined{
        const configItem:boolean | undefined = this._configuration.get(key);
        if(!this._configuration || !configItem){
            vscode.window.showErrorMessage('Configuration '+ key + ' is not correctly set, please review it.');
            return;
        }
        return configItem;
    }

    getSonarQubeRootUrl():string | undefined{
        return this.getConfigItem('Sonarqube-url');
    }

    getSonarQubeToken():string | undefined{
        return this.getConfigItem('Sonarqube-token');
    }

    getSonarQubeProjectkey():string | undefined{ 
        return this.getConfigItem('Sonarqube-projectkey');
    }
    
    getRulesetRelativepath():string | undefined{
        return this.getConfigItem('ruleset-relativepath', true);
    }
        
    getSonarlintCompatibilityMode():boolean{
         return this.getConfigItemBoolean('sonarlint-compatibility-mode') ?? true;
    }

    getNugetOrgRootUrl(): string {
        return "https://www.nuget.org";
    }
}
