import * as vscode from 'vscode';
import { injectable } from "inversify";
import { IUIInteractions } from "./IUIInteractions";

@injectable()
export class VsCodeUIInteractions implements IUIInteractions{
    showErrorMessage(message: string) {
        vscode.window.showErrorMessage(message);
    }
    showInformationMessage(message: string){
        vscode.window.showInformationMessage(message);
    }
    showWarningMessage(message: string){
        vscode.window.showWarningMessage(message);
    }
}