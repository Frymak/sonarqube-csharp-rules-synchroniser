# sonarqube-rules-synchroniser for C# VSCode extension

## Features

This extensions will allow you to :

* Automatically retrieve the C# ruleset for your project on SonarQube (at workspace opening or when starting the "Synchronize C# rules" command)
* Automatically install SonarQube analysers for C# (omnisharp) plugin and automatically enable Roslyn analysers (SonarQube analysers are based on Roslyn).
* Automatically update SonarQube analysers for C# (omnisharp) plugin.

## Preview version

* As it is a preview version of this extension, it may contain bugs and unwanted behaviors. This extension has been tested the most in sonarlint compatibility mode, so prefer use this option for now.
* As the code is available on github, do not hesitate to do a PR with your fix/new feature, I'll try to add it asap.
* This extension only support SonarQube onpremise installation and not SaaS sonarsource mode... (and it is not planned, if you want this, you can do some PRs).

## Requirements

This extension requires C# extension from Omnisharp to be installed ("ms-dotnettools.csharp").

## Extension Settings

This extension has the following settings:

* `sonarqube-rules-synchroniser.sonarlint-compatibility-mode`: Defines if the ruleset will be stored as sonarlint does within VisualStudio. Default is true. If enabled, this feature will try to provide you compatibility between SonarLint on VisualStudio and this extension behavior. If enabled, all csproj projects will be modified, if not Directory.Build.props file will be used
* `sonarqube-rules-synchroniser.Sonarqube-url`: Defines the Url to use to call SonarQube
* `sonarqube-rules-synchroniser.Sonarqube-token`: Defines the token to use to call SonarQube
* `sonarqube-rules-synchroniser.Sonarqube-projectkey`: Defines the project key to search rules for. **This setting has to be configured at workspace level**, if not, the same project rules will be applied to all your workspaces...
* `sonarqube-rules-synchroniser.ruleset-relativepath`: Defines the ruleset file relative path
* `sonarqube-rules-synchroniser.autosynchronize-on-startup`: Defines if the ruleset is automatically synchronized on vscode startup. (Default set to true)
* `sonarqube-rules-synchroniser.autoconfigure-csharp-analyzers`: Defines if this extension forces csharp analyzers. If set to true, this extension will force setting on omnisharp plugin to ensure Roslyn Analyzers are enabled and will dowwload locally the SonarQube analyzers nuget. (Default set to true)

## Release Notes

### 0.0.4

Ensure previous unused analysers folders are removed

### 0.0.3

Update 'ruleset-relativepath' parameter documentation.
Add extension tags for marketplace visibility.

### 0.0.2

Update default parameters 'autoconfigure-csharp-analyzers' & 'autosynchronize-on-startup' are now set to true by default.

### 0.0.1

Initial release of SonarQube-rules-synchroniser

**Enjoy!**
