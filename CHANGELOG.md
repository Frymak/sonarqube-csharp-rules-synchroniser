# Change Log

## Release Notes

### 0.0.4

Ensure previous unused analysers folders are removed

### 0.0.3

Update 'ruleset-relativepath' parameter documentation.
Add extension tags for marketplace visibility.

### 0.0.2

Update default parameters 'autoconfigure-csharp-analyzers' & 'autosynchronize-on-startup' are now set to true by default.

### 0.0.1

Initial release of SonarQube-rules-synchroniser
